Entrega del proyecto inicial

Fecha de entrega: 12 nov 2023


Documentos a entregar por separado (SIN HACER UN ARCHIVO COMPRIMIDO):

    - Documentación del proceso de desarrollo en pdf (Redacción del proceso que se ha llevado a cabo para completar el proyecto que podrá contener como figuras los diagramas y deberá contar con una tabla de contenidos)
    - Archivo .pdf con el diagrama ER
    - Archivo .pdf que contenga el diagrama relacional
    - Archivo sql con la creación de tablas y restricciones
    - Copia de seguridad de la base de datos con datos ya introducidos
    - Archivo con la configuración de la generación de los datos (.dgen)
    - Archivo .dbd con el diagrama generado automáticamente
    - Archivo .pdf de la documentación generada automáticamente

Os recuerdo que la base de datos deberá tener un nombre significativo y que las relaciones deberán estar normalizadas desde el paso a tablas.
Subir la tarea como un archivo comprimido será constitutivo de nota = 0
